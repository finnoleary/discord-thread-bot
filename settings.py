"""
Settings for Bot Boy
"""

from typing import NoReturn
import json
# from jsonschema import validate

# This version won't validate things, because I'm not sure how to handle
# the errors with this yet :<
SETTINGS_SCHEMA = {
    'type': 'object',
    'additionalProperties': False,
    'patternProperties': {
        '^.*$': {  # "guild id cast to string"
            'type': 'object',
            'properties': {
                'name': {'type': 'string'},
                'category': {'type': 'int'},
                'delay': {'type': 'number'},
                'inactivity': {
                    'type': 'object',
                    'properties': {
                        'monitoring': {'type': 'bool'},
                        'grace_period': {'type': 'string'},
                        'archival_category': {'type': 'int'},
                    }
                }
            }
        }
    }
}


class BotBoyState():
    def __init__(self, log: object, config_path, default_delay=30):
        self.log = log
        self.default_delay = default_delay
        self.config_path = config_path
        self.state = {}

    def save(self) -> NoReturn:
        self.log.info("Saving state...")
        with open(self.config_path, "w") as h:
            h.write(json.dumps(self.state, indent=4))
            self.log.info("State saved.")

    def load(self) -> NoReturn:
        self.log.info("Loading state...")
        if self.config_path.exists():
            with open(self.config_path, "r") as h:
                # this is expensive, doing this every time we save/load.
                # Maybe we can take a delta or something.
                new_state = json.load(h)
                self.state = {int(key): value
                              for key, value in new_state.items()}
                self.log.info("State loaded.")

    def list_guild_ids(self) -> list:
        return list(self.state.keys())

    def add_guild(self, guild: object) -> NoReturn:
        if guild.id not in self.state:
            self.state[guild.id] = {
                'name': guild.name,
                'category': -1,
                'delay': self.default_delay,
                'inactivity': {
                    'monitoring': False
                }
            }

    def remove_guild(self, guild: object) -> NoReturn:
        self.state.pop(guild.id)

    def remove_guild_by_id(self, guild_id: int) -> NoReturn:
        self.state.pop(guild_id)

    def set_category(self, guild: object, category_id: int) -> NoReturn:
        self.state[guild.id]['category'] = category_id

    def get_category(self, guild: object) -> int:
        return self.state[guild.id]['category']

    def set_delay(self, guild: object, delay: int) -> NoReturn:
        self.state[guild.id]['delay'] = delay

    def get_delay(self, guild: object) -> int:
        return self.state[guild.id]['delay']

    def set_monitoring(self, guild: object, monitoring: bool) -> NoReturn:
        self.state[guild.id]['inactivity']['monitoring'] = monitoring

    def get_monitoring(self, guild: object) -> bool:
        return self.state[guild.id]['inactivity']['monitoring']

    def set_grace_period(self, guild: object, period: str) -> NoReturn:
        self.state[guild.id]['inactivity']['grace_period'] = period

    def get_grace_period(self, guild: object) -> str:
        return self.state[guild.id]['inactivity']['grace_period']

    def set_archival_category(self, guild: object, category: int) -> NoReturn:
        self.state[guild.id]['inactivity']['archival_category'] = category

    def get_archival_category(self, guild: object) -> int:
        return self.state[guild.id]['inactivity']['archival_category']
